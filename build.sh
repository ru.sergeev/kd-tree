#!/usr/bin/env bash
git pull
git submodule update --init --recursive
make clean
rm -f /p/a/t/h CMakeCache.txt
cmake CMakeLists.txt
make
tst/kdtree_test
bin/build_kdtree --file input/sample_data.csv
bin/query_kdtree --file input/sample_data.csv --tree input/sample_data.csv.kd --query input/query_data.csv --out out.csv