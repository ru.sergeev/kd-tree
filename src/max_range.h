//
// Created by Dmitry Sergeev on 10/9/16.
//

#ifndef KD_TREE_MAX_RANGE_H
#define KD_TREE_MAX_RANGE_H

#include "kd_set.h"

#include <tuple>
#include <algorithm>
#include <vector>

/// pivot strategy: choses splitting coordinate based on gtreater variance
/// defines a coordinate chosing behaviour
/// can be selected in --pivot option
struct max_range{
	template<typename value_t>
	size_t pivot(std::shared_ptr<kd_set<value_t>> set) {
        static_assert(std::is_scalar<value_t>::value, "value_t must be scalar type");
        auto k = set->dimensions();
        std::vector<value_t> ranges(k);
#pragma omp parallel for
        for (auto split = 0; split < k; split++) {
            auto min = std::min_element(set->begin(), set->end(), set->compare(split));
            auto max = std::max_element(set->begin(), set->end(), set->compare(split));
            ranges[split] = set->at(*max, split)-set->at(*min, split);
        }
        auto max_range = std::max_element(ranges.begin(), ranges.end());
        return std::distance(ranges.begin(), max_range);
	}
};

#endif //KD_TREE_MAX_RANGE_H
