//
// Created by Dmitry Sergeev on 10/6/16.
//

#ifndef KD_TREE_RANDOM_PIVOT_STRATEGY_H
#define KD_TREE_RANDOM_PIVOT_STRATEGY_H

#include <tuple>
#include <random>

/// pivot strategy: random pic coordinate, and the splitting point: [WARNING] potentially leads to unbalanced trees
/// defines a coordinate chosing behaviour
/// can be selected in --pivot option
struct random_pick {
	template<typename set_t>
    size_t pivot(std::shared_ptr<set_t> set) {
        auto k = set->dimensions();
        std::random_device rd;
        std::mt19937 gen(rd());
        std::uniform_int_distribution<> coordinate_distribution(0, k - 1);
        return coordinate_distribution(gen);
	}
};

#endif //KD_TREE_RANDOM_PIVOT_STRATEGY_H
