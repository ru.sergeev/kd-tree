//
// Created by Dmitry Sergeev on 10/1/16.
//

// main build_kdtree application:
// uses option_parser to parse the parameter
// uses csv to load kd_vector
// uses kd_tree to build and save the tree

// usage: build_kdtree --file <csv file name> [--pivot { cycle_in_order|max_variance|random_pick|median_of_medians } ]
// [ --ext <output file extension>]


#include "option_parser.h"
#include "csv.h"
#include "kd_tree.h"
#include "kd_vector.h"
#include "pivot_strategy.h"
#include "builder.h"

#include <iostream>

/// main build_kdtree function
int main(const int argc, const char *argv[]) {
    option_parser input;

    // kd_set a command line arguments contract
    input.requires(input_file, "file");
    input.handles(option_list, "pivot", "cycle_in_order,max_variance,max_range,random_pick");
    input.handles(output_file, "out");

    // parse parameters
    std::string message = input.parse(argc, argv);
    if (message != "ok") { // arguments don't mach the contract
        std::cout << message << std::endl;
        std::cout << input.usage_suggestion() << std::endl;
        return 1; // exit
    }

    typedef double value_t;

    auto data_filename = input.get("file");
    std::cout << "reading data from " << data_filename << "...";

    csv file(input.get("file"));
    auto data = file.read<value_t>();

	std::cout << "ok" << std::endl;
    std::cout <<  data.dimensions()<< "D : count = " << data.size() << std::endl;
    std::cout << "building a tree,  ";

    builder factory;
    std::unique_ptr<kd_tree<value_t>> tree;

   // choosing the pivot strategy template
    std::cout << "pivot strategy is ";
    if (input.has("pivot")) {
		auto pivot_strategy = input.get("pivot");
        std::cout << pivot_strategy << "..." << std::endl;
		if (pivot_strategy == "cycle_in_order") {
            tree = factory.make_tree(std::move(data), cycle_in_order());
		} else if (pivot_strategy == "max_variance") {
            tree = factory.make_tree(std::move(data), max_variance());
		} else if (pivot_strategy == "max_range") {
            tree = factory.make_tree(std::move(data), max_range());
        } else if (pivot_strategy == "random_pick") {
            tree = factory.make_tree(std::move(data), random_pick());
		} } else {
		// default strategy: can be defined at compile time
        std::cout << "default (see compile options)...";
        tree = factory.make_tree(std::move(data), default_pivot_strategy());
	}
    std::cout << "ok" << std::endl;

	std::string output_filename;
    output_filename = input.has("out") ?
    			input.get("out") :
				input.get("file") + ".kd";

	std::cout << "saving tree structure to "<< output_filename << "...";

	std::ofstream output_file(output_filename);
	tree->save(output_file);

    std::cout << "ok" << std::endl;
    std::cout << "the " << data_filename << " is required to load kd-tree" << std::endl;
    std::cout << std::endl << "done." << std::endl;

    return 0;
}