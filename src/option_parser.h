//
// Created by Dmitry Sergeev on 10/6/16.
//

// command line option parser
// parses command line argument as a kd_set of keys and values:
// --key [value] ..
// supports required and optional keys

#ifndef KD_TREE_OPTION_PARSER_H
#define KD_TREE_OPTION_PARSER_H

#include <map>
#include <vector>
#include <tuple>
#include <string>

/// defines types of command line arguments
enum option_type {input_file, output_file, option_list};

/// helps to parse command line options
class option_parser
{
public:
    /// sert required command line argument
    void requires(option_type option, const std::string & feature, const std::string & parameters = "");
    /// sets optional command line argument
    void handles(option_type option, const std::string & feature, const std::string & parameters = "");
	/// parse input to main function
    std::string parse(const int& argc, const char** argv);
    /// returns value for an option: --option value
	std::string get(const std::string& feature);
    /// chech if a specific --option was entered
	bool has(const std::string& feature) const;
    /// constructs a string for suggesting the application usage format
	std::string usage_suggestion() const;
private:
	static std::tuple<bool, std::string> parse_key(const std::string& arg);
	static bool check_file(std::string filename);
	static bool check_filename(std::string filename);
    bool check_list(std::string input, std::string list) const;
private:
    std::vector < std::string> required;
	std::map < std::string, std::tuple<option_type, std::string>> handled;
	std::map < std::string, std::string> received;
	std::string program_name;
};

#endif //KD_TREE_OPTION_PARSER_H
