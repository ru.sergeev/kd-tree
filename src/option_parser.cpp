//
// Created by Dmitry Sergeev on 10/6/16.
//

// command line option parser (implementation)

#include "option_parser.h"

#include <algorithm>
#include <fstream>
#include <sstream>
#include <iostream>
#include <cassert>

void option_parser::requires(option_type option, const std::string &feature, const std::string &parameters) {
    required.push_back(feature);
    handles(option, feature, parameters);
}

void option_parser::handles(option_type option, const std::string &feature, const std::string &parameters) {
    handled[feature] = std::make_tuple(option, parameters);
}
std::string option_parser::parse(const int& argc, const char** argv) {
	std::string last_option = "";
    program_name = std::string(argv[0]);
    auto slash_position = program_name.find_last_of("\\/");
    if (slash_position != std::string::npos)
        program_name = program_name.substr(slash_position+1);
	for (auto i = 1; i < argc; i++){
		bool option;
		std::string feature;
		std::tie(option, feature) = parse_key(argv[i]);
		if (option){
			received[feature] = "";
			last_option = feature;
		}
		else{
			if (last_option == ""){
				return "incorrect format: no option for " + feature;
			}
			if (received[last_option] != ""){
				return "incorrect format: multiple inputs for option --" + received[last_option];
			}
			received[last_option] = feature;
		}
	}
	for (const auto& option : required){
		if (received.find(option) == received.end()) {
			return "missing option: --" + option;
		}
	}
	for (auto pair : received){
		if (handled.find(pair.first) == handled.end()) {
			return "unknown option: --" + pair.first;
		}
	}
	for (auto pair : received){
        bool check_pass = false;
        option_type option;
        std::string parameter;
        std::tie(option, parameter) = handled[pair.first];
        switch(option){
            case input_file:
                check_pass = check_file(pair.second);
                break;
            case output_file:
                check_pass = check_filename(pair.second);
                break;
            case option_list:
                check_pass = check_list(pair.second, parameter);
                break;
        default:
			assert("unsupported option choice");
        	break;
        }
        if (!check_pass) {
            return "incorrect input: --" + pair.first + " " + pair.second;
        }
	}
	return "ok";
}

std::tuple<bool, std::string> option_parser::parse_key(const std::string& arg){
	std::string prefixes[]{"--", "-", "//"};
	for (auto prefix:prefixes) {
		if (arg.compare(0, prefix.size(), prefix) == 0) {
			return std::make_tuple(true, arg.substr(prefix.size()));
		}
	}
	return std::make_tuple(false, arg);
}

std::string option_parser::get(const std::string& feature){
    assert(handled.find(feature) != handled.end());
	return received[feature];
}

bool option_parser::has(const std::string& feature) const{
    assert(handled.find(feature) != handled.end());
	return received.find(feature) != received.end();
}

std::string option_parser::usage_suggestion() const{
    std::string usage = "usage: " + program_name;
    for (const auto& pair : handled) {
        auto option = pair.first;
        if (std::find(required.begin(), required.end(), option) != required.end()){
            usage += " --" + option + " <" + std::get<1>(pair.second) + ">";
        }else{
            usage += " [--" + option + " <" + std::get<1>(pair.second) + ">]";
        }
    }
    return usage;
}

bool option_parser::check_file(std::string filename)
{
    std::ifstream file(filename);
    auto success = file.is_open();
    if (!success)
        std::cout << "cannot open file" << '\n';
    file.close();
    return success;
}

bool option_parser::check_filename(std::string filename)
{
    if (filename == "") {
        std::cout << "filename for output cannot be empty\n";
        return false;
    }
    auto forbiddenChars = {'\\', ':', '?', '\"', '<', ',', '>', '|' };
    for(auto c : forbiddenChars) {
        if (filename.find(c) != std::string::npos){
            std::cout << "filename could not have the following characters \\/:?\"<,>|\n";
            return false;
        }
    }
    return true;
}

bool option_parser::check_list(std::string input, std::string list) const
{
    std::istringstream lineStream(list);
    std::string value;
    while(getline(lineStream, value, ',')) {
        if (input == value) {
            return true;
        }
    }
    std::cout << "the input \"" + input + "\" is not supported\n";
    return false;
}
