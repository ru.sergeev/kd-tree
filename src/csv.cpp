//
// Created by Dmitry Sergeev on 10/11/16.
//

#include "csv.h"

#include <cassert>
#include <algorithm>

csv::csv(std::string filename): file( std::unique_ptr<std::ifstream>(new std::ifstream(filename))){
    assert(file->is_open());
    std::string line;
    getline(*file, line);
    _columns = count_columns(line);
    file->clear();
    file->seekg(0, std::ios::beg);
}

size_t csv::columns() const{
    return _columns;
}

std::size_t csv::count_columns(std::string line){
    if (line.empty()) return 0;
    return std::count(line.begin(), line.end(), ',') + 1;
}
