//
// Created by Dmitry Sergeev on 10/11/16.
//

#ifndef KD_TREE_TREE_H
#define KD_TREE_TREE_H

#include "kd_vector.h"

#include <memory>
#include <fstream>
#include <ostream>
#include <istream>
#include <cassert>
#include <cmath>
#include <algorithm>

/// the KD-tree class
template< typename value_t >
class kd_tree {
    static_assert(std::is_scalar<value_t>::value, "value_t must be scalar type");
	typedef kd_vector<value_t> container_t;
public:
    struct node {
        const int index;
        const int split;
        const std::shared_ptr<struct node>left;
        const std::shared_ptr<struct node> right;
        /// constructs a node with subnodes - for recurcive initialization
        node(const int index, const int split, std::shared_ptr<struct node> left, std::shared_ptr<struct node> right)
                : index(index), split(split), left(std::move(left)), right(std::move(right)) {}
        /// serializes a subtree to a stream
        friend std::ostream &operator<<(std::ostream &os, const std::shared_ptr<struct node> &node) {
            os << node->index << ',' << node->split << ',';
            if (node->left != nullptr ) os <<  node->left;
            else os <<  '*';
            os <<  ',';
            if (node->right != nullptr ) os <<  node->right;
            else os <<  '*';
            return os;
        }
        /// serializes a subtree to a stream
        friend std::istream &operator>>(std::istream &is,  std::shared_ptr<struct node> &node) {
            std::string value;
            getline(is, value, ',');
            if (value == "*") {
                node = std::shared_ptr<struct node>(nullptr);
                return is;
            }
            auto index = std::stoi(value);
            getline(is, value, ',');
            auto split = std::stoi(value);
            std::shared_ptr<struct node> left;
            std::shared_ptr<struct node> right;
            is >> left;
            is >> right;
            node = std::make_shared<struct node>(index, split, left, right);
            return is;
        }
    };
public:
	/// initialize KD-tree with KD-vector
    kd_tree( std::shared_ptr<container_t> data, std::shared_ptr<node> root) : data (data), root(std::move(root)) {}
    /// initialize KD-tree with KD-vector and load tree structure
    kd_tree( container_t&& data, std::ifstream &is) : data ( std::make_shared<container_t>(data)) {
        is >> root;
    }
	/// saves kd-tree to a stream
    void save(std::ostream &os)  {
        os << root;
    }
	/// find nearest points:
	/// returns answer in ready to write format
    container_t make_query(const container_t&& move_query) const {
        auto query = std::make_shared<container_t>(move_query);
		std::vector<std::size_t> index(query->size());
		std::vector<value_t> distance_square(query->size());
#pragma omp parallel for
		for (auto i = 0; i < query->size(); i++) {
			std::tie(index[i], distance_square[i]) = find_nearest(query, i);
		}
        std::vector<value_t> data;
		for (auto i = 0; i < query->size(); i++) {
            data.emplace_back(index[i]);
            data.emplace_back(std::sqrt(distance_square[i]));
		}
        container_t result(2, std::move(data));
		return result;
	}
    /// finds a nearest to a target point in the trea
    /// returns index in the data and distance to the tree
    std::tuple<size_t, value_t> find_nearest(const std::shared_ptr<container_t> query, const std::size_t target) const {
        return find_nearest(root, query, target, std::numeric_limits<value_t>::max());
    }
private:
    std::tuple<size_t, value_t> find_nearest(const std::shared_ptr<struct node> node,
                                             const std::shared_ptr<container_t> query,
                                             const std::size_t target,
                                             value_t min_distance) const{
        auto candidate = 0xbadbadbad;
        if (node == nullptr) return std::make_tuple(candidate, std::numeric_limits<value_t>::max());
        auto distance = distance_sq(node->index, query, target);
        if ( distance < min_distance){ // current node is good candidate
            min_distance = distance;
            candidate = node->index;
        }
        if (node->split == -1)  return std::make_tuple(candidate, distance);
        auto coordinate_position = delta(node->index, query, target, node->split);
        const std::shared_ptr<struct node> nearest = (coordinate_position > 0) ? node->left : node->right;
        const std::shared_ptr<struct node> further = (coordinate_position > 0) ? node->right : node->left;
        std::size_t index;
        // look in nearest
        std::tie(index, distance) = find_nearest(nearest, query, target, min_distance);
        if (distance < min_distance) {
            min_distance = distance;
            candidate = index;
        }
        // look in further
        if( coordinate_position*coordinate_position < min_distance) {// but cut too far hyper-rectangles
            std::tie(index, distance) = find_nearest(further,  query, target, min_distance);
            if (distance < min_distance) {
                min_distance = distance;
                candidate = index;
            }
        }
        return std::make_tuple(candidate, min_distance);
    }
    /// square distance to the target in other container
    value_t distance_sq(std::size_t index, std::shared_ptr<container_t> query, std::size_t target) const {
        assert( data->dimensions() == query->dimensions() );
        value_t result = 0;
#pragma omp parallel for
        for (auto i = 0; i < data->dimensions(); i++){
            auto dX= delta(index, query, target, i);
            result += dX*dX;
        }
        return result;
    }
    /// square delta between index and the target in other container on a coordinate
    value_t delta (std::size_t index, std::shared_ptr<container_t> query, std::size_t target, std::size_t coordinate) const {
        assert( data->dimensions() == query->dimensions() );
        return data->at(index, coordinate)-query->at(target, coordinate);
    }
private:
    std::shared_ptr<container_t> data;
    std::shared_ptr<node> root;
};

#endif //KD_TREE_TREE_H
