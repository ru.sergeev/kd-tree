//
// Created by Dmitry Sergeev on 10/1/16.
//

#include "option_parser.h"
#include "csv.h"
#include "kd_vector.h"
#include "kd_tree.h"

#include <iostream>

/// main function
int main(const int argc, const char *argv[]) {
    option_parser input;

    // kd_set a command line arguments contract
    input.requires(input_file, "tree");
    input.requires(input_file, "file");
    input.requires(input_file, "query");
    input.handles(output_file, "out");

    // parse parameters
    std::string message = input.parse(argc, argv);
    if (message != "ok") { // arguments don't mach the contract
        std::cout << message << std::endl;
        std::cout << input.usage_suggestion() << std::endl;
        return 1; // exit
    }

    typedef double value_t;

    // read the csv file to a data frame
    std::cout << "reading data from " << input.get("file") << "...";

    csv file(input.get("file"));
    auto data = file.read<value_t>();

    std::cout << "ok" << std::endl;
    std::cout << data.dimensions()<< "D : count = " << data.size() << std::endl;
    std::cout << "reading tree structure from " << input.get("tree") << "...";

    std::ifstream tree_file(input.get("tree"));
    kd_tree<value_t> tree(std::move(data), tree_file);

    std::cout << "ok" << std::endl;
    std::cout << "reading query data from " << input.get("query") << "...";

    csv file_query(input.get("query"));
    auto query = file_query.read<value_t>();

    std::cout << "ok" << std::endl;
    std::cout << query.dimensions()<< "D : count = " << query.size() << std::endl;

    if (data.dimensions() != query.dimensions()){
        std::cout << "hey! dimension does not mach. bailing out..." << std::endl;
        return 1; // exit
    }

    std::cout << "performing query of " << query.size() << " points in the tree ...";

    auto result = tree.make_query(std::move(query));

    std::cout << "ok" << std::endl;
    std::string output_filename = input.has("out")? input.get("out") : "out.csv";
    std::cout << "saving indices and distances to " << output_filename << "...";

    std::ofstream output_file(output_filename);
    output_file << result;

    std::cout << "ok" << std::endl;
    std::cout << std::endl << "done." << std::endl;

	return 0;
}
