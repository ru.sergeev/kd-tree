//
// Created by Dmitry Sergeev on 10/11/16.
//

#ifndef KD_TREE_SET_H
#define KD_TREE_SET_H

#include "kd_vector.h"
#include "comparator.h"

#include <memory>
#include <vector>
#include <numeric>

/// kd_set is a array of indeces. all grouping and sorting operations are done
/// on objects of this class, the real data in kd_vector remains.
/// see comparator class
template <typename value_t>
class kd_set{
    static_assert(std::is_scalar<value_t>::value, "value_t must be scalar type");
	typedef std::vector<std::size_t> vector_t;
    typedef kd_vector<value_t> container_t;
public:
	/// initial constructor
    kd_set( std::shared_ptr<container_t> data) : data(  data  ), _begin( 0 ), _end(data->size()) {
        vec = std::make_shared<std::vector<size_t>>(data->size());
        std::iota(vec->begin(), vec->end(), 0); // Fill with 0, n-1
    }
	/// split constructor
	kd_set(std::shared_ptr<container_t> data, std::shared_ptr<vector_t> vec, std::size_t begin, std::size_t end)
		: data(data), vec(vec), _begin(begin), _end(end) {}
	/// left hyper-rectangle
    std::shared_ptr<kd_set> left( std::size_t index ) const {
        return std::make_shared<kd_set>(data, vec, _begin, _begin + index); }
	/// right hyper-rectangle
    std::shared_ptr<kd_set> right( std::size_t index ) const {
        return std::make_shared<kd_set>(data, vec, _begin + index + 1, _end); }
	/// provide comparator for sorting operations
    comparator<container_t> compare(size_t split){
        return comparator<container_t>(data, split); }
	/// iterator through the set
    typedef vector_t::iterator iterator;
    iterator begin(){ return vec->begin() + _begin; }
    iterator end() { return vec->begin() + _end; }
    std::size_t dimensions() { return data->dimensions();}
	std::size_t size() { return _end - _begin;}
	value_t at(size_t direct_index, size_t split) const {
		return data->at(direct_index, split);
	}
private:
    std::shared_ptr<container_t> data;
    std::shared_ptr<vector_t> vec;
    std::size_t _begin;
	std::size_t _end;
};

#endif //KD_TREE_SET_H
