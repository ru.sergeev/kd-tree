//
// Created by Dmitry Sergeev on 10/11/16.
//

#ifndef KD_TREE_COMPARATOR_H
#define KD_TREE_COMPARATOR_H

#include <memory>

/// comparator provides sorting capabilities for kd_vector and through kd_set
/// so the kd_set cab be sorted on definite coordinate by standard functions
/// i.e. nth_element
template<typename container_t>
struct comparator
{
    bool operator()( const std::size_t & lhs, const std::size_t & rhs) const{
        if (auto data_shared = data.lock()) {
            auto left = data_shared->at(lhs, coordinate);
            auto right = data_shared->at(rhs, coordinate);
            return left < right;
        }else
            throw "data vector is dead";
    }
    comparator(std::weak_ptr<container_t>  data , std::size_t coordinate)
            : data(data), coordinate(coordinate) {}
private:
    const std::weak_ptr<container_t> data;
    const std::size_t coordinate;
};

#endif //KD_TREE_COMPARATOR_H
