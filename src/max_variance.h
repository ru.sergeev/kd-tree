//
// Created by Dmitry Sergeev on 10/6/16.
//

#ifndef KD_TREE_MAX_VARIANCE_PIVOT_STRATEGY_H
#define KD_TREE_MAX_VARIANCE_PIVOT_STRATEGY_H

#include "kd_set.h"

#include "kd_vector.h"
#include <vector>
#include <tuple>
#include <algorithm>

/// pivot strategy: choses splitting coordinate based on gtreater variance
/// defines a coordinate chosing behaviour
/// can be selected in --pivot option
struct max_variance{
	template<typename value_t>
	std::size_t pivot(std::shared_ptr<kd_set<value_t>> set) {
		static_assert(std::is_scalar<value_t>::value, "value_t must be scalar type");
		auto k = set->dimensions();
		auto N = set->size();
		std::vector<value_t> variance(k);
#pragma omp parallel for
		for (auto split = 0; split < k; split++) {
			variance[split] = calculate_variance(set, split);
		}
		auto max_variance = std::max_element(variance.begin(), variance.end());
		return std::distance(variance.begin(), max_variance);
	}
private:
    template<typename value_t>
    value_t calculate_variance(std::shared_ptr<kd_set<value_t>> set, std::size_t split){
        size_t N = set->size();
        auto s2 = value_t(0);
        auto s = value_t(0);
#pragma omp parallel for
        for(auto it = set->begin(); it < set->end(); ++it){
            auto x = set->at(*it, split);
            s += x;
            s2 += x*x;
        }
        return (N*s2 - s*s) / (N*(N - 1));
    }
};
#endif //KD_TREE_MAX_VARIANCE_PIVOT_STRATEGY_H
