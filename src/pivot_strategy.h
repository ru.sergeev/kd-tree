//
// Created by Dmitry Sergeev on 10/6/16.
//

// strategy pattern for pivoting kd-space
// duck interface to all strategies
// handles compile-time default strategy selection
// fail-safes default strategy if not defined
// supports command line choice of a pivoting

#ifndef KD_TREE_PIVOT_STRATEGY_H
#define KD_TREE_PIVOT_STRATEGY_H

#include "cycle_in_order.h"
#include "max_variance.h"
#include "random_pick.h"
#include "max_range.h"

// the preprocessor code reminds during compile time about setting the default pivot strategy for kd-tree build

#ifndef default_pivot_strategy
#define STR_HELPER(x) #x
#define STR(x) STR_HELPER(x)
#define default_pivot_strategy cycle_in_order
#pragma message ("default_pivot_strategy was not kd_set in CMake. Setting up to " STR(default_pivot_strategy))
#undef STR
#undef STR_HELPER
#endif

#endif //KD_TREE_PIVOT_STRATEGY_H
