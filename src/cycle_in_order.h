//
// Created by Dmitry Sergeev on 10/6/16.
//

#ifndef KD_TREE_IN_ORDER_PIVOT_STRATEGY_H
#define KD_TREE_IN_ORDER_PIVOT_STRATEGY_H

#include "kd_set.h"

#include <tuple>
#include <memory>

/// pivot strategy: cycle_in_order - sequentially iterate through coordinates 0..k-1
/// defines a coordinate chosing behaviour
/// can be selected in --pivot option
struct cycle_in_order{
    template<typename value_t>
    std::size_t pivot(std::shared_ptr<kd_set<value_t>> set) const {
        static_assert(std::is_scalar<value_t>::value, "value_t must be scalar type");
        split = (split + 1) % set->dimensions();
        return split;
    }
private:
    mutable std::size_t split = -1;
};

#endif //KD_TREE_IN_ORDER_PIVOT_STRATEGY_H
