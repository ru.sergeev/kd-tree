//
// Created by Dmitry Sergeev on 10/11/16.
//

#ifndef KD_TREE_BUILDER_H
#define KD_TREE_BUILDER_H

#include "kd_set.h"
#include "kd_tree.h"
#include "pivot_strategy.h"
#include "kd_vector.h"

#include <memory>
#include <numeric>
#include <algorithm>
#include <tuple>
#include <vector>

/// the template class builds a kd-tree using default or requested strategy

class builder {
public:
	template <typename value_t, typename pivot_strategy = default_pivot_strategy> //std::shared_ptr<>
	std::unique_ptr<kd_tree<value_t>> make_tree( kd_vector<value_t> && vector, pivot_strategy strategy = pivot_strategy()) const {
        static_assert(std::is_scalar<value_t>::value, "value_t must be scalar type");
        auto data = std::make_shared<kd_vector<value_t>>(vector);
        auto set = std::make_shared<kd_set<value_t>>(data);
        auto nodes = split_set<value_t>(set, strategy);
        return std::unique_ptr<kd_tree<value_t>>( new kd_tree<value_t>( data, std::move(nodes) ) );
    }
private:
	template<typename value_t, typename pivot_strategy>
	std::shared_ptr<typename kd_tree<value_t>::node> split_set(std::shared_ptr<kd_set<value_t>> set, pivot_strategy strategy) const {
        static_assert(std::is_scalar<value_t>::value, "value_t must be scalar type");
		if (set->end() == set->begin()) { return nullptr; }
		if (set->end() - set->begin() == 1)
			return std::make_shared<typename kd_tree<value_t>::node>(*set->begin(), -1, nullptr, nullptr);
		std::size_t split = strategy.pivot(set);
        std::size_t index = set->size() / 2;
		std::nth_element(set->begin(), set->begin() + index, set->end(), set->compare(split));
		auto left_set = split_set(set->left(index), strategy) ;
		auto right_set = split_set(set->right(index), strategy) ;
		return std::make_shared<typename kd_tree<value_t>::node>(*(set->begin() + index), split, left_set, right_set);
	}
};

#endif //KD_TREE_BUILDER_H
