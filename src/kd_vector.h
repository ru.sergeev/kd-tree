//
// Created by Dmitry Sergeev on 10/11/16.
//

#ifndef KD_TREE_VECTOR_H
#define KD_TREE_VECTOR_H

#include <vector>
#include <cassert>
#include <memory>
#include <ostream>

/// kd_vector - product of csv read and main data container
/// template on data type: float, double int, etc
/// stores data in a single vector - motivation is speed
/// reshapes through yielding a kd_point - point in kd-space - for i-th element
template< typename value_t>
class kd_vector{
    static_assert(std::is_scalar<value_t>::value, "value_t must be scalar type");
    typedef std::vector<value_t> container_t;
public:
	/// initilizeses dementions and a real container (vector)
    kd_vector(size_t dimensions, container_t && data) : k(dimensions), data(std::make_shared<container_t>(data)) {}
	/// accessor to a point at demention
    value_t at( size_t index, size_t split ) const {
        assert(split < k);
        return data->at( index * k + split );
    }
	/// kd-vector size
    size_t size(){ return data->size() / k; }
	/// number of dimensions (K)
    std::size_t dimensions() const { return k; }

    friend std::ostream &operator<<(std::ostream &os, const kd_vector &vector) {
        for (auto i = 0; i < vector.data->size(); i++){
            os <<  vector.data->at(i);
            if((i+1) % vector.k)  os << ',';
            else os << std::endl;;
        }
        return os;
    }
private:
    size_t k;
    std::shared_ptr<container_t> data;
};

#endif //KD_TREE_VECTOR_H
