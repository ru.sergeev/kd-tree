﻿#pragma once


#include "kd_vector.h"

#include <istream>
#include <sstream>
#include <string>
#include <fstream>
#include <memory>

// reads file
// outputs to a container - like kd_vector factory
class csv
{
public:
	csv(std::string filename);
	size_t columns() const;
    template<typename value_t>
    kd_vector<value_t> read(){
        static_assert(std::is_scalar<value_t>::value, "value_t must be scalar type");
        std::vector<value_t> vec;
        value_t value;
        std::string line;
        while (getline(*file, line)) {
            std::string record;
            std::stringstream current_line(line);
            while(getline(current_line, record, ',')){
                std::stringstream rs(record);
                rs >> value;
                vec.emplace_back(value);
            }
        }
        kd_vector<value_t> result(columns(), std::move(vec));
        return result;
    };
private:
	static std::size_t count_columns(std::string line);
private:
	std::unique_ptr<std::ifstream> file;
	std::size_t _columns;
};
