# KD­Tree

## Short task description

The task is to develop a KDtree 
library that provides at least the
following interface/capabilities:

❏ Create a tree from a set of K-dimensional
points. The tree should be created by recursively choosing
a good axis (dimension) and position to split on. 

Choose an appropriate measure of “goodness” (e.g.
largest range, variance, cycle) and justify your decision. 
Likewise for the split position (e.g. median, median of medians).

❏ Support efficient exact query for the nearest stored point to a query point. 
That is, search through the tree efficiently and return an iterator 
or reference to the nearest point.

❏ Support I/O to save/load the tree from disk.

❏ Implementation is templated on the scalar type of the point 
such that it supports at least `float` and `double` precision types.

## Solution:

### Build instructions:

You need Git, CMake and a compiler to build it on vanilla Ubuntu 14.04 LTS.

Also the solution uses Google Tests for unit testing. So after cloning a repository, you need to get submodules.

Finally, please runn CMake and make to build twon application `build_kdtree`,  `query_kdtree` and test `kdtree`.

> sudo apt-get install git cmake build-essential

> git clone --recursive https://gitlab.com/ru.sergeev/kd-tree.git

> cd kd-tree

> cmake CMakeLists.txt

> make

Run `./build.sh` to perform build, test and run. You still require apt-get packages and clone repository.

Note:

You can change a default pivoting stategy for splitting the data on tree building by modifying **add_definitions(-Ddefault_pivot_strategy=`max_variance`)** in the root CMakeList.txt.
However, you can also specify `--pivot` option along with desired pivoting method name to the **build_kdtree**.

### Run test:

Tests builds as a part of solution. Run them without parameters.

> tst/kdtree_test

### Run instructions:

Build a tree:
> bin/build_kdtree --file input/sample_data.csv 

Performe a query: 
> bin/query_kdtree --file input/sample_data.csv --tree input/sample_data.csv.kd --query input/query_data.csv --out out.csv
 
 See result:
> cat out.csv 
 
 More options:
    
    build_kdtree --file <> [--out <>] [--pivot <cycle_in_order|max_variance|max_range|random_pick>]
    
    query_kdtree --file <> [--out <>] --query <> --tree <>
 
##File Formats
The file format is CSV with a comma delimiter:

    X00, X01, X02, ... X0N
    X10, X11, X12, ... X1N
    ...
    
Each line contains data for a single point. 

The point ID is the 0-based row number.
The application takes in a set of query points and produce a file output that consists for each query
point, the corresponding closest index of the sample data and the Euclidean distance from the query point
to the nearest neighbor:

    query0_closest_index, query0_minimum_distance
    query1_closest_index, query1_minimum_distance
    ...
 
 


