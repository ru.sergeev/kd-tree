//
// Created by Dmitry Sergeev on 10/10/16.
//

#include <ostream>
#include <memory>
#include "gtest/gtest.h"

#include "csv.h"


struct csv_open : testing::Test
{
    std::shared_ptr<csv> file;
};

struct csv_state {
    std::string filename;
    int columns;
};

struct open_csv_test : csv_open, testing::WithParamInterface<csv_state>
{
    open_csv_test()
    {
        file = std::make_shared<csv>(GetParam().filename);
    }
};

TEST_P(open_csv_test, filename)
{
    auto state = GetParam();
    EXPECT_EQ(state.columns, file->columns());
};

INSTANTIATE_TEST_CASE_P(Default, open_csv_test,
                        testing::Values(
                                csv_state{ "input/sample_data.csv", 3},
                                csv_state{ "input/query_data.csv", 3}
                        ));