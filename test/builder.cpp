//
// Created by Dmitry Sergeev on 10/16/16.
//

#include "gtest/gtest.h"

#include "builder.h"
#include <string>
#include <iostream>
#include <sstream>


struct builder_works: testing::Test {
    std::unique_ptr<kd_tree<int>> tree;
    bool check_tree(const std::string & serialized) const{
        int counter = 0;
        for(auto c: serialized){
            if (c == ',') counter ++;
            if (c == '*') counter -= 2;
            if (counter < 0) return false;
        }
        return counter == 1;
    }
    int count_nodes(const std::string & serialized) const{
        std::stringstream line(serialized);
        std::string record;
        int count = 0;
        while(getline(line, record, ',')){
            std::stringstream rs(record);
            if (record != "*" ) count++;
        }
        return count / 2;
    }
};

struct builder_state {
    std::vector<int> vector;
    int dimensions;
    std::string pivot_strategy;
    std::string serialized;
};

struct builder_test : builder_works, testing::WithParamInterface<builder_state> {
    builder_test() {
        auto state = GetParam();
        builder factory;
        std::vector<int> vec;
        std::copy(state.vector.begin(), state.vector.end(), std::back_inserter(vec));
        kd_vector<int> data(state.dimensions, std::move(vec));
        if (state.pivot_strategy == "cycle_in_order") {
            tree = factory.make_tree(std::move(data), cycle_in_order());
        } else if (state.pivot_strategy == "max_variance") {
            tree = factory.make_tree(std::move(data), max_variance());
        } else if (state.pivot_strategy == "max_range") {
            tree = factory.make_tree(std::move(data), max_range());
        } else if (state.pivot_strategy == "random_pick") {
            tree = factory.make_tree(std::move(data), random_pick());
        } else
            throw "need a good pivot strategy";

    }
};

TEST_P(builder_test, tree_partitioning){
    auto state = GetParam();
    if(state.pivot_strategy == "random_pick") return;
    std::stringstream ss;
    tree->save(ss);
    EXPECT_EQ(state.serialized, ss.str());
};

TEST_P(builder_test, tree_corectness){
    auto state = GetParam();
    std::stringstream ss;
    tree->save(ss);
    EXPECT_TRUE(check_tree(ss.str()));
};

TEST_P(builder_test, tree_node_count){
    auto state = GetParam();
    std::stringstream ss;
    tree->save(ss);
    EXPECT_EQ(state.vector.size() / state.dimensions, count_nodes(ss.str()));
};

INSTANTIATE_TEST_CASE_P(Default, builder_test,
                        testing::Values(
                                // 1D sort
                                builder_state{ {10,2,6,9,3,1}, 2, "cycle_in_order", "1,0,2,-1,*,*,0,-1,*,*" },
                                builder_state{ {10,2,6,9,3,1}, 2, "max_variance", "0,1,2,-1,*,*,1,-1,*,*" },
                                builder_state{ {10,2,6,9,3,1}, 2, "max_range", "0,1,2,-1,*,*,1,-1,*,*" },
                                builder_state{ {10,2,6,9,3,1}, 2, "random_pick", "every time different" },
                                builder_state{ {10,2,6,9,30,1}, 2, "max_range", "0,0,1,-1,*,*,2,-1,*,*" }
                                // todo: add more

                        ));