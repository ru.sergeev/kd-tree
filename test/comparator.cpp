//
// Created by Dmitry Sergeev on 10/15/16.
//

#include "gtest/gtest.h"
#include "comparator.h"
#include "kd_vector.h"
#include "kd_set.h"

#include <vector>
#include <algorithm>
#include <ostream>

struct comparator_works: testing::Test {
    std::shared_ptr<kd_vector<int>> kd_vec;
    std::shared_ptr<kd_set<int>> set;
    std::vector<int> projection;
};

struct vector_state {
    std::vector<int> vector;
    int dimensions;
    int sort_by;
    std::vector<int> result;
};

struct comparator_test : comparator_works, testing::WithParamInterface<vector_state> {
    comparator_test() {
        std::vector<int> vec;
        std::copy(GetParam().vector.begin(), GetParam().vector.end(), std::back_inserter(vec));
        kd_vec = std::make_shared<kd_vector<int>>(GetParam().dimensions, std::move(vec));
        set = std::make_shared<kd_set<int>>(kd_set<int>(kd_vec));
        std::sort(set->begin(), set->end(), set->compare(GetParam().sort_by));
        for(auto index : *set){
            for(auto split = 0; split < set->dimensions(); split++)
                projection.push_back(set->at(index, split));
        }
    }
};

TEST_P(comparator_test, sort){
    auto state = GetParam();
    EXPECT_EQ(state.result, projection);
};

INSTANTIATE_TEST_CASE_P(Default, comparator_test,
                        testing::Values(
                                // 1D sort
                                vector_state{ {10,2,6,8,3,1}, 1, 0, {1,2,3,6,8,10}},
                                // 2D sort by 0-coordinate
                                vector_state{ {10,2 ,6,8, 3,1}, 2, 0, {3,1, 6,8, 10,2}},
                                // 2D sort by 1-coordinate
                                vector_state{ {10,2, 6,8, 3,1}, 2, 1, {3,1, 10,2, 6,8}},
                                // 3D sort by 0-coordinate
                                vector_state{ {10,2,6, 8,3,1}, 3, 0, {8,3,1, 10,2,6}},
                                // 3D sort by 1-coordinate
                                vector_state{ {10,2,6, 8,3,1}, 3, 1, {10,2,6, 8,3,1}},
                                // 3D sort by 2-coordinate
                                vector_state{ {10,2,6, 8,3,1}, 3, 0, {8,3,1, 10,2,6}}
                        ));