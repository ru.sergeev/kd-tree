//
// Created by Dmitry Sergeev on 10/16/16.
//

#include "gtest/gtest.h"

#include "kd_vector.h"
#include "kd_set.h"
#include "max_variance.h"

#include <algorithm>

struct max_variance_works: testing::Test {
    std::shared_ptr<kd_set<double>> set;
    max_variance strategy;
    int split;
};

struct max_variance_state {
    std::vector<double> vector;
    int dimensions;
    int split;
};

struct max_variance_test : max_variance_works, testing::WithParamInterface<max_variance_state> {
    max_variance_test() {
        auto state = GetParam();
        std::vector<double> vec;
        std::copy(state.vector.begin(), state.vector.end(), std::back_inserter(vec));
        kd_vector<double> data(state.dimensions, std::move(vec));
        set = std::make_shared<kd_set<double>>(std::make_shared<kd_vector<double>>(std::move(data)));
        split = strategy.pivot(set);
    }
};

TEST_P(max_variance_test, cycling){
    auto state = GetParam();
    EXPECT_EQ(state.split, split);
};

INSTANTIATE_TEST_CASE_P(Default, max_variance_test,
                        testing::Values(
                                // 1D sort
                                max_variance_state{ {10,2,6,9,3,1, 0, 10}, 1, 0},
                                max_variance_state{ {10,2, 6,9, 3,1, 0,0, 10,10}, 2, 1},
                                max_variance_state{ {10,2,6, 9,3,1, 0,0,0, 10,10,10}, 3, 0},
                                max_variance_state{ {7,2,2, 2,0,10, 4,0,10, 1,8,5, 7,9,8, 0,0,0, 10,10,10}, 3, 1},
                                max_variance_state{ {7,2,2, 2,0,10, 4,0,10, 1,8,5, 7,9,8, 0,0,0, 10,10,10 }, 3, 1},
                                max_variance_state{ {7,2,2, 2,0,10, 4,0,10, 1,8,5, 7,9,8, 0,0,0, 10,10,10}, 3, 1},
                                max_variance_state{ {7,2,2, 2,0,10, 4,0,10, 1,8,5, 7,9,18, 0,0,0, 18,18,18}, 3, 2},
                                max_variance_state{ {7,2,2, 2,0,10, 4,0,10, 1,8,5, 7,19,8, 0,0,0, 19,19,19}, 3, 1},
                                max_variance_state{ {7,2,2, 2,0,10, 4,0,10, 1,8,5, 17,9,8, 0,0,0, 17,17,17}, 3, 0},
                                max_variance_state{ {1,2,3, 1,2,3, 1,2,3, 1,2,3, 3,2,3, 0,0,0, 3,3,3}, 3, 0},
                                max_variance_state{ {1,2,3, 1,2,3, 1,2,3, 1,2,3, 1,0,3, 0,0,0, 3,3,3}, 3, 1},
                                max_variance_state{ {1,2,3, 1,2,3, 1,2,3, 1,2,3, 1,2,1, 0,0,0, 3,3,3}, 3, 2},
                                max_variance_state{ {1,2,3, 1,2,3, 1,2,3, 1,2,3, 1,2,3, 1,2,3, 1,2,3}, 3, 0},
                                max_variance_state{ {1,1,3, 1,2,3, 1,1,3, 1,2,3, 1,2,3, 1,2,3, 1,2,3}, 3, 1},
                                max_variance_state{ {1,2,1, 1,2,3, 1,2,3, 1,2,3, 1,2,3, 1,2,3, 1,2,3}, 3, 2}
                        ));