//
// Created by Dmitry Sergeev on 10/10/16.
//

#include <ostream>
#include <memory>
#include "gtest/gtest.h"

#include "csv.h"
#include "kd_vector.h"

struct csv_read : testing::Test
{
	std::shared_ptr<csv> file;
    std::shared_ptr<kd_vector<double>> data;
};

struct csv_state {
    friend std::ostream &operator<<(std::ostream &os, const csv_state &state) {
        os << "filename: " << state.filename << " size: " << state.size << " dimensions: " << state.dimensions;
        return os;
    }
    std::string filename;
    int size;
    int dimensions;
};

struct read_csv_test : csv_read, testing::WithParamInterface<csv_state>
{
    read_csv_test()
    {
		file = std::make_shared<csv>(GetParam().filename);
		data = std::make_shared<kd_vector<double>>(file->read<double>());
    }
};

TEST_P(read_csv_test, filename)
{
    auto state = GetParam();
    EXPECT_EQ(state.size, data->size());
    EXPECT_EQ(state.dimensions, data->dimensions());
};

INSTANTIATE_TEST_CASE_P(Default, read_csv_test,
                        testing::Values(
                                csv_state{ "input/sample_data.csv", 1000, 3},
                                csv_state{ "input/query_data.csv", 1000, 3}
                        ));