//
// Created by Dmitry Sergeev on 10/16/16.
//

#include "gtest/gtest.h"

#include "kd_tree.h"
#include "builder.h"
#
#include <algorithm>

struct query_works: testing::Test {
    std::unique_ptr<kd_tree<double>> tree;
    builder factory;
    int split;
    int index;
    double distance;
};

struct query_state {
    std::vector<double> vector;
    int dimensions;
    std::string pivot_strategy;
    std::vector<double> query;
    int index;
    double distance;
};

struct query_test : query_works, testing::WithParamInterface<query_state> {
    query_test() {
        auto state = GetParam();
        std::vector<double> vec;
        std::copy(state.vector.begin(), state.vector.end(), std::back_inserter(vec));
        kd_vector<double> data(state.dimensions, std::move(vec));
        if (state.pivot_strategy == "cycle_in_order") {
            tree = factory.make_tree(std::move(data), cycle_in_order());
        } else if (state.pivot_strategy == "max_variance") {
            tree = factory.make_tree(std::move(data), max_variance());
        } else if (state.pivot_strategy == "max_range") {
            tree = factory.make_tree(std::move(data), max_range());
        } else if (state.pivot_strategy == "random_pick") {
            tree = factory.make_tree(std::move(data), random_pick());
        } else
            throw "need a good pivot strategy";
        std::vector<double> vec2;
        std::copy(state.query.begin(), state.query.end(), std::back_inserter(vec2));
        kd_vector<double> query(state.dimensions, std::move(vec2));
        std::tie(index, distance) = tree->find_nearest(std::make_shared<kd_vector<double>>(std::move(query)), 0);
    }
};

TEST_P(query_test, index){
    auto state = GetParam();
    EXPECT_EQ(state.index, index);
};

TEST_P(query_test, dintance){
    auto state = GetParam();
    EXPECT_EQ(state.distance, distance);
};

INSTANTIATE_TEST_CASE_P(Default, query_test,
                        testing::Values(
                                query_state{ {10,2,6,9,3,1}, 1, "cycle_in_order", {0}, 5, 1.0},
                                query_state{ {10,2, 6,9, 3,1}, 2, "cycle_in_order", {2,1}, 2, 1.0},
                                query_state{ {10,2,6, 9,3,1}, 3, "cycle_in_order", {0,0,0}, 1, 91.0},
                                query_state{ {7,2,2, 2,0,10, 4,0,10, 1,8,5, 7,9,8}, 3, "cycle_in_order", {7,2,2}, 0, 0.0},
                                query_state{ {7,2,2, 2,0,10, 4,0,10, 1,8,5, 7,9,8}, 3, "cycle_in_order", {2,0,10}, 1, 0.0},
                                query_state{ {7,2,2, 2,0,10, 4,0,10, 1,8,5, 7,9,8}, 3, "cycle_in_order", {4,0,10}, 2, 0.0},
                                query_state{ {7,2,2, 2,0,10, 4,0,10, 1,8,5, 7,9,8}, 3, "cycle_in_order", {1,8,5}, 3, 0.0},
                                query_state{ {7,2,2, 2,0,10, 4,0,10, 1,8,5, 7,9,8}, 3, "cycle_in_order", {7,9,8}, 4, 0.0},
                                query_state{ {7,2,2, 2,0,10, 4,0,10, 1,8,5, 7,9,8}, 3, "cycle_in_order", {2,2,2}, 0, 25.0},
                                query_state{ {7,2,2, 2,0,10, 4,0,10, 1,8,5, 7,9,8}, 3, "cycle_in_order", {2,1,10}, 1, 1.0},
                                query_state{ {7,2,2, 2,0,10, 4,0,10, 1,8,5, 7,9,8}, 3, "cycle_in_order", {4,10,10}, 4, 14.0},
                                query_state{ {7,2,2, 2,0,10, 4,0,10, 1,8,5, 7,9,8}, 3, "cycle_in_order", {10,8,5}, 4, 19.0},
                                query_state{ {7,2,2, 2,0,10, 4,0,10, 1,8,5, 7,9,8}, 3, "cycle_in_order", {7,0,8}, 2, 13.0},
                                query_state{ {7,2,2, 2,0,10, 4,0,10, 1,8,5, 7,9,8}, 3, "max_variance", {2,2,2}, 0, 25.0},
                                query_state{ {7,2,2, 2,0,10, 4,0,10, 1,8,5, 7,9,8}, 3, "max_variance", {2,1,10}, 1, 1.0},
                                query_state{ {7,2,2, 2,0,10, 4,0,10, 1,8,5, 7,9,8}, 3, "max_variance", {4,10,10}, 4, 14.0},
                                query_state{ {7,2,2, 2,0,10, 4,0,10, 1,8,5, 7,9,8}, 3, "max_variance", {10,8,5}, 4, 19.0},
                                query_state{ {7,2,2, 2,0,10, 4,0,10, 1,8,5, 7,9,8}, 3, "max_variance", {7,0,8}, 2, 13.0},
                                query_state{ {7,2,2, 2,0,10, 4,0,10, 1,8,5, 7,9,8}, 3, "max_range", {2,2,2}, 0, 25.0},
                                query_state{ {7,2,2, 2,0,10, 4,0,10, 1,8,5, 7,9,8}, 3, "max_range", {2,1,10}, 1, 1.0},
                                query_state{ {7,2,2, 2,0,10, 4,0,10, 1,8,5, 7,9,8}, 3, "max_range", {4,10,10}, 4, 14.0},
                                query_state{ {7,2,2, 2,0,10, 4,0,10, 1,8,5, 7,9,8}, 3, "max_range", {10,8,5}, 4, 19.0},
                                query_state{ {7,2,2, 2,0,10, 4,0,10, 1,8,5, 7,9,8}, 3, "max_range", {7,0,8}, 2, 13.0},
                                query_state{ {7,2,2, 2,0,10, 4,0,10, 1,8,5, 7,9,8}, 3, "max_variance", {7,2,2}, 0, 0.0},
                                query_state{ {7,2,2, 2,0,10, 4,0,10, 1,8,5, 7,9,8}, 3, "max_variance", {2,0,10}, 1, 0.0},
                                query_state{ {7,2,2, 2,0,10, 4,0,10, 1,8,5, 7,9,8}, 3, "max_variance", {4,0,10}, 2, 0.0},
                                query_state{ {7,2,2, 2,0,10, 4,0,10, 1,8,5, 7,9,8}, 3, "max_variance", {1,8,5}, 3, 0.0},
                                query_state{ {7,2,2, 2,0,10, 4,0,10, 1,8,5, 7,9,8}, 3, "max_variance", {7,9,8}, 4, 0.0}
                        ));