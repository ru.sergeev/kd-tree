//
// Created by Dmitry Sergeev on 10/16/16.
//

#include "gtest/gtest.h"

#include "kd_vector.h"
#include "kd_set.h"
#include "max_range.h"

#include <algorithm>

struct max_range_works: testing::Test {
    std::shared_ptr<kd_set<int>> set;
    max_range strategy;
    int split;
};

struct max_range_state {
    std::vector<int> vector;
    int dimensions;
    int split;
};

struct max_range_test : max_range_works, testing::WithParamInterface<max_range_state> {
    max_range_test() {
        auto state = GetParam();
        std::vector<int> vec;
        std::copy(state.vector.begin(), state.vector.end(), std::back_inserter(vec));
        kd_vector<int> data(state.dimensions, std::move(vec));
        set = std::make_shared<kd_set<int>>(std::make_shared<kd_vector<int>>(std::move(data)));
        split = strategy.pivot(set);
    }
};

TEST_P(max_range_test, cycling){
    auto state = GetParam();
    EXPECT_EQ(state.split, split);
};

INSTANTIATE_TEST_CASE_P(Default, max_range_test,
                        testing::Values(
                                // 1D sort
                                max_range_state{ {10,2,6,9,3,1}, 1, 0},
                                max_range_state{ {10,2, 6,9, 3,1}, 2, 1},
                                max_range_state{ {10,2,6, 9,3,1}, 3, 2},
                                max_range_state{ {7,2,2, 2,0,10, 4,0,10, 1,8,5, 7,9,8}, 3, 1},
                                max_range_state{ {7,2,2, 2,0,10, 4,0,10, 1,8,5, 7,9,8}, 3, 1},
                                max_range_state{ {7,2,2, 2,0,10, 4,0,10, 1,8,5, 7,9,8}, 3, 1},
                                max_range_state{ {7,2,2, 2,0,10, 4,0,10, 1,8,5, 7,9,18}, 3, 2},
                                max_range_state{ {7,2,2, 2,0,10, 4,0,10, 1,8,5, 7,19,8}, 3, 1},
                                max_range_state{ {7,2,2, 2,0,10, 4,0,10, 1,8,5, 17,9,18}, 3, 0}
                        ));