//
// Created by Dmitry Sergeev on 10/16/16.
//

#include "gtest/gtest.h"

#include "kd_vector.h"
#include "kd_set.h"
#include "cycle_in_order.h"

#include <algorithm>

struct cycle_works: testing::Test {
    std::shared_ptr<kd_set<int>> set;
    cycle_in_order strategy;
    std::vector<int> split;
};

struct cycle_state {
    std::vector<int> vector;
    int dimensions;
    int cycles;
    std::vector<int> split;
};

struct cycle_test : cycle_works, testing::WithParamInterface<cycle_state> {
    cycle_test() {
        auto state = GetParam();
        std::vector<int> vec;
        std::copy(state.vector.begin(), state.vector.end(), std::back_inserter(vec));
        kd_vector<int> data(state.dimensions, std::move(vec));
        set = std::make_shared<kd_set<int>>(std::make_shared<kd_vector<int>>(std::move(data)));
        for(auto i = 0; i < state.cycles; i++){
            split.push_back(strategy.pivot(set));
        }
    }
};

TEST_P(cycle_test, cycling){
    auto state = GetParam();

    EXPECT_EQ(state.split, split);
};

INSTANTIATE_TEST_CASE_P(Default, cycle_test,
                        testing::Values(
                                // 1D sort
                                cycle_state{ {10,2,6,9,3,1}, 1, 4, {0,0,0,0}},
                                cycle_state{ {10,2,6,9,3,1}, 2, 4, {0,1,0,1}},
                                cycle_state{ {10,2,6,9,3,1}, 3, 4, {0,1,2,0}}
                        ));